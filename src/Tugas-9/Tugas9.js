import React from 'react';
import './Tugas9.css';

class Tugas9 extends React.Component {
  render() {
    return (
      <>
        {
          <div className="Tugas9">
            <div className="form">
              <h1>Form Pembelian Buah</h1>
              <form>
                <table>
                  <tr>
                    <td>
                      <label>
                        Nama Pelanggan
                      </label>
                    </td>
                    <td>
                      <input type="text" name="namaPelanggan" />
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <label>
                        Daftar Item
                      </label>
                    </td>
                    <td>
                      <input type="checkbox" name="daftarItem" id="daftarItem" />
                      <label for="daftarItem">Semangka</label>
                      <br></br>
                      <input type="checkbox" name="daftarItem" id="daftarItem" />
                      <label for="daftarItem">Jeruk</label>
                      <br></br>
                      <input type="checkbox" name="daftarItem" id="daftarItem" />
                      <label for="daftarItem">Nanas</label>
                      <br></br>
                      <input type="checkbox" name="daftarItem" id="daftarItem" />
                      <label for="daftarItem">Salak</label>
                      <br></br>
                      <input type="checkbox" name="daftarItem" id="daftarItem" />
                      <label for="daftarItem">Anggur</label>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <input type="submit" value="Kirim" className="tombol" />
                    </td>
                  </tr>
                </table>
              </form>
            </div>
          </div>
        }
      </>
    );
  }
}

export default Tugas9;
