import React from 'react';
import './App.css';
import Tugas9 from './Tugas-9/Tugas9';
import Tugas10 from './Tugas-10/Tugas10';

function App() {
  return (
    <div className="App">
      <Tugas9 />
      <Tugas10 />
    </div>
  );
}

export default App;
