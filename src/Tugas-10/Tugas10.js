import React from 'react';
import './Tugas10.css';

let dataHargaBuah = [
    {nama: "Semangka", harga: 10000, berat: 1000},
    {nama: "Anggur", harga: 40000, berat: 500},
    {nama: "Strawberry", harga: 30000, berat: 400},
    {nama: "Jeruk", harga: 30000, berat: 1000},
    {nama: "Mangga", harga: 30000, berat: 500}
];

class DataTabel extends React.Component {
    render() {
        return <td>{this.props.nama}</td>;
    }
}

class Tugas10 extends React.Component {
    render() {
        return(
            <>
                <h1>Tabel Harga Buah</h1>
                <table>
                    <th>Nama</th>
                    <th>Harga</th>
                    <th>Berat</th>
                {
                    dataHargaBuah.map(
                        element=> {
                            return(
                                <tr>
                                    <DataTabel nama={element.nama}/>
                                    <DataTabel nama={element.harga}/>
                                    <DataTabel nama={(element.berat/1000) + " kg"}/>
                                </tr>
                            )
                        }
                    )
                }
                </table>
            </>
        )
    }
}

export default Tugas10;